using Blazor.Auth;
using Database;
using Database.Model;
using Database.UnitOfWork;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Blazor;

public class Startup
{
    private IConfiguration Configuration { get; }
        
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddAuthenticationCore();
        services.AddRazorPages();
        services.AddServerSideBlazor();
        services.AddSingleton<PasswordHasher<Employee>>();
        services.AddScoped<AuthenticationStateProvider, CustomAuthenticationStateProvider>();
            
        var connectionString = Configuration.GetConnectionString("AZURE_SQL_CONNECTION_STRING"); 
        services.AddDbContextFactory<LiftContext>(options => options.UseSqlServer(connectionString));
        services.AddDbContext<LiftContext>(options => options.UseSqlServer(connectionString));
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseForwardedHeaders(new ForwardedHeadersOptions
        {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
        });
        
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapBlazorHub();
            endpoints.MapFallbackToPage("/_Host");
        });

        MigrateDatabase(app);
        SeedDefaultManager(app);
    }
    
    private static void MigrateDatabase(IApplicationBuilder app)
    {
        using var scope = app.ApplicationServices.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<LiftContext>();
        dbContext.Database.Migrate();
    }
    
    private static void SeedDefaultManager(IApplicationBuilder app)
    {
        using var scope = app.ApplicationServices.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<LiftContext>();
        
        using var uow = UnitOfWorkFactory.Create(dbContext);
        var managers = uow.EmployeeRepository.GetMultiple(employee => employee.Role == Role.Manager);

        if (managers.Any())
        {
            return;
        }
        
        var hasher = scope.ServiceProvider.GetRequiredService<PasswordHasher<Employee>>();
        var defaultManager = new Employee
        {
            Username = "manager", 
            Password = "manager", 
            Role = Role.Manager,
            Name = "",
            Address = "",
            Phone = "",
            Email = ""
        }; 
        defaultManager.Password = hasher.HashPassword(defaultManager, defaultManager.Password);
        
        uow.EmployeeRepository.Create(defaultManager);
        uow.Commit();
    }
}