using System.Security.Claims;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;

namespace Blazor.Auth;

public class CustomAuthenticationStateProvider : AuthenticationStateProvider
{
    private readonly ProtectedSessionStorage _sessionStorage;
    private readonly ClaimsPrincipal _quest = new(new ClaimsIdentity());

    public CustomAuthenticationStateProvider(ProtectedSessionStorage sessionStorage)
    {
        _sessionStorage = sessionStorage;
    }

    public override async Task<AuthenticationState> GetAuthenticationStateAsync()
    {
        try
        {
            var sessionStorageResult = await _sessionStorage.GetAsync<UserSession>("UserSession");
            var session = sessionStorageResult.Success ? sessionStorageResult.Value : null;

           if (session == null)
           { 
               return await Task.FromResult(new AuthenticationState(_quest));
           }
        
            var claims = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>
            {
                new(ClaimTypes.Name, session.Username),
                new(ClaimTypes.Role, session.Role)
            }, "CustomAuth"));

            return await Task.FromResult(new AuthenticationState(claims));
        }
        catch
        {
            return await Task.FromResult(new AuthenticationState(_quest));
        }
    }

    public async Task UpdateAuthenticationState(UserSession? session)
    {
        ClaimsPrincipal claimsPrincipal;

        if (session == null)
        {
            await _sessionStorage.DeleteAsync("UserSession");
            claimsPrincipal = _quest;
        }
        else
        {
            await _sessionStorage.SetAsync("UserSession", session);
            claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>
            {
                new(ClaimTypes.Name, session.Username),
                new(ClaimTypes.Role, session.Role)
            }));
        }
        
        NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(claimsPrincipal)));
    }
}