<a name="readme-top"></a>
[![LinkedIn][linkedin-shield]][linkedin-url]

<div align="center">

  <h3 align="center">iLift</h3>

  <p align="center">
    A simple fitness centre information system for receptions<br />
    <a href="https://ilift.apps.dungeon.software/">View Demo</a>
  </p>
</div>

## About The Project

A simple fitness centre information system for receptions

Here's what it can for do:
* The system provides two roles - managers and employees
* You can create, edit and delete customers 
* You can manage their permit subscriptions 
* You can manage products and record their sales
* Additionally, managers can manage employee profiles and view sale statistics

This project was created thanks to the courses PB175 and PV178 in Brno at FI MUNI.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Built With

[![.NET][DotNet.com]][DotNet-url] [![Blazor][Blazor.com]][Blazor-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

You are free to clone the repository and change appsetting.json connection string to your own database.
However, I strongly suggest using Docker for deployment.

The program automatically runs migrations and seeds the database with manager account.

### Docker setup
#### Prerequisites

You need to have Docker installed on your system. For further details refer to the [docker documentation](https://docs.docker.com/get-docker/).

#### Running the app 

The container expects only a connection string to database in ADO.NET format. One such string example:

*Server=tcp:your-namespace.database.windows.net,1433;Initial Catalog=ilift-database;Persist Security Info=False;User ID=userid;Password=your_password;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;*

1. Running the container 
   ```sh
   docker run -p 8080:80 dungeonsoftware/ilift:latest -e 'ConnectionStrings:AZURE_SQL_CONNECTION_STRING'=<your-connection-string>
   ```
   
2. Initially, the system gets seeded with one manager account. You can log in with the following credentials:
   ```
   username: manager
   password: manager
   ```
   
<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->
## Contact

Stanislav Zeman - [standa.dev](https://standa.dev)

Project Link: [https://gitlab.com/dungeon-software/ilift](https://gitlab.com/dungeon-software/ilift)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/standa-zeman

[Blazor.com]: https://camo.githubusercontent.com/c8b610b9cf2320754d67c7e2e403569591e5baff7d4184ce75fbf94b49765f23/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d426c617a6f7226636f6c6f723d353132424434266c6f676f3d426c617a6f72266c6f676f436f6c6f723d464646464646266c6162656c3d
[Blazor-url]: https://dotnet.microsoft.com/en-us/apps/aspnet/web-apps/blazor
[DotNet.com]: https://camo.githubusercontent.com/ff765790707ecba41b57071db549f75fbf0eeffa5ac6996ff077083863b8bea4/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d2e4e455426636f6c6f723d353132424434266c6f676f3d2e4e4554266c6f676f436f6c6f723d464646464646266c6162656c3d
[DotNet-url]: https://dotnet.microsoft.com/
