using Database.Model;
using Database.Repository;

namespace Database.UnitOfWork;

public class UnitOfWork : IUnitOfWork
{
    public IRepository<Employee> EmployeeRepository { get; }
    public IRepository<Product> ProductRepository { get; }
    public IRepository<Customer> CustomerRepository { get; }
    public IRepository<SoldProduct> SoldProductRepository { get; }
    public IRepository<Permit> PermitRepository { get; }
    private readonly LiftContext _dbContext;

    public UnitOfWork(LiftContext dbContext)
    {
        _dbContext = dbContext;
        ProductRepository= new ProductRepository(_dbContext);
        EmployeeRepository = new EmployeeRepository(_dbContext);
        CustomerRepository = new CustomerRepository(_dbContext);
        SoldProductRepository = new SoldProductsRepository(_dbContext);
        PermitRepository = new PermitRepository(_dbContext);
    }

    public void Commit()
    {
        _dbContext.SaveChanges();
    }

    public void Dispose()
    {
        _dbContext.Dispose();
    }
}