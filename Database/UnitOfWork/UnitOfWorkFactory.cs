namespace Database.UnitOfWork;

public static class UnitOfWorkFactory
{
    public static IUnitOfWork Create(LiftContext dbContext)
    {
        return new UnitOfWork(dbContext);
    }
}