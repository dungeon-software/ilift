using Database.Model;
using Database.Repository;

namespace Database.UnitOfWork;

public interface IUnitOfWork : IDisposable
{
    public IRepository<Employee> EmployeeRepository { get; }
    public IRepository<Product> ProductRepository { get; }
    public IRepository<Customer> CustomerRepository { get; }
    public IRepository<SoldProduct> SoldProductRepository { get; }
    public IRepository<Permit> PermitRepository { get; }

    void Commit();
}