﻿using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database;

public class LiftContext : DbContext
{
    public DbSet<Employee> Employees => Set<Employee>();
    public DbSet<Product> Products => Set<Product>();
    public DbSet<Customer> Customers => Set<Customer>();
    public DbSet<SoldProduct> SoldProducts => Set<SoldProduct>();
    public DbSet<Permit> Permits => Set<Permit>();

    public LiftContext(DbContextOptions<LiftContext> options) : base(options)
    {
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Customer>()
            .Property(c => c.Email)
            .IsRequired();

        modelBuilder.Entity<Employee>()
            .Property(e => e.Username)
            .IsRequired();
       
        modelBuilder.Entity<Employee>() 
            .Property(e => e.Password)
            .IsRequired();
        
        modelBuilder.Entity<Employee>()
            .Property(e => e.Email)
            .IsRequired();

        modelBuilder.Entity<SoldProduct>()
            .Property(sp => sp.SoldAt)
            .HasDefaultValue(DateTime.Now);

        modelBuilder.Entity<SoldProduct>()
            .Property(sp => sp.SoldAt)
            .HasDefaultValue(DateTime.Now.AddYears(1));

        modelBuilder.Entity<SoldProduct>()
            .Property(sp => sp.AmountSold)
            .HasDefaultValue(1);
    }
}