using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Model;

[Table("SoldProducts")]
public class SoldProduct
{
    public int Id { get; set; }
    [Required]
    public Product Product { get; set; }
    [Required]
    public int AmountSold { get; set; }
    [Required]
    public Employee SoldBy { get; set; }
    [Required]
    public DateTime SoldAt { get; set; }
}