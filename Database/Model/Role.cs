namespace Database.Model;

public enum Role
{
    Manager,
    Employee
}