using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Model;

[Table("Permits")]
public class Permit
{
    public int Id { get; set; }
    [Required]
    public Customer Owner { get; set; }
    [Required]
    public int PermitNumber { get; set; }
    [Required]
    public DateTime ExpirationDate { get; set; }
    [Required]
    public DateTime IssueDate { get; set; }
    public int? EntriesLeft { get; set; }
}