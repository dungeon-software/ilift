using System.Linq.Expressions;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.Repository;

public class EmployeeRepository : IRepository<Employee>
{
    private readonly LiftContext _context;
    private DbSet<Employee> DbSet => _context.Employees;
    
    public EmployeeRepository(LiftContext context)
    {
        _context = context;
    }
    
    public void Create(Employee entity)
    {
        DbSet.Add(entity);
    }

    public void Edit(Employee entity)
    {
        DbSet.Update(entity);
    }

    public Employee? GetOne(int id)
    {
        return DbSet.Find(id);
    }
    public Employee? GetOne(string username)
    {
        return DbSet.FirstOrDefault(employee => employee.Username == username);
    }

    public IEnumerable<Employee> GetMultiple(
        Expression<Func<Employee, bool>>? filter = null, 
        Func<IQueryable<Employee>, IOrderedQueryable<Employee>>? orderBy = null)
    {
        IQueryable<Employee> query = DbSet;
        
        if (filter != null)
        {
            query = query.Where(filter);
        }

        if (orderBy != null)
        {
            return orderBy(query).ToList();
        }

        return query.ToList();
    }

    public void Delete(int id)
    {
        var entity = DbSet.Find(id);

        if (entity == null)
        {
            return;
        }
        
        entity.DeletedAt = DateTime.Now;
        
        Edit(entity);
    }
}