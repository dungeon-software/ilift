using System.Linq.Expressions;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.Repository;

public class ProductRepository : IRepository<Product>
{
    private readonly LiftContext _context;
    private DbSet<Product> DbSet => _context.Products;

    public ProductRepository(LiftContext context)
    {
        _context = context;
    }
    
    public void Create(Product entity)
    {
        DbSet.Add(entity);
    }

    public void Edit(Product entity)
    {
        DbSet.Update(entity);
    }

    public Product? GetOne(int id)
    {
        return DbSet.Find(id);
    }

    public IEnumerable<Product> GetMultiple(
        Expression<Func<Product, bool>>? filter = null, 
        Func<IQueryable<Product>, IOrderedQueryable<Product>>? orderBy = null) 
    {
        IQueryable<Product> query = DbSet;
        
        if (filter != null)
        {
            query = query.Where(filter);
        }

        if (orderBy != null)
        {
            return orderBy(query).ToList();
        }

        return query.ToList();
    }

    public void Delete(int id)
    {
        var entity = DbSet.Find(id);

        if (entity == null)
        {
            return;
        }

        entity.DeletedAt = DateTime.Now;
        
        Edit(entity);
    }
}