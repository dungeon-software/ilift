using System.Linq.Expressions;

namespace Database.Repository;

public interface IRepository<T>
{
    void Create(T entity);

    void Edit(T entity);
    
    T? GetOne(int id);

    IEnumerable<T> GetMultiple(
        Expression<Func<T, bool>>? filter = null,
        Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null);

    void Delete(int id);
}