using System.Linq.Expressions;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.Repository;

public class SoldProductsRepository : IRepository<SoldProduct>
{
    private readonly LiftContext _context;
    private DbSet<SoldProduct> DbSet => _context.SoldProducts;

    public SoldProductsRepository(LiftContext context)
    {
        _context = context;
    }
    
    public void Create(SoldProduct entity)
    {
        DbSet.Add(entity);
    }

    public void Edit(SoldProduct entity)
    {
        DbSet.Update(entity);
    }

    public SoldProduct? GetOne(int id)
    {
        return DbSet.Find(id);
    }

    public IEnumerable<SoldProduct> GetMultiple(
        Expression<Func<SoldProduct, bool>>? filter = null, 
        Func<IQueryable<SoldProduct>, IOrderedQueryable<SoldProduct>>? orderBy = null)
    {
        IQueryable<SoldProduct> query = DbSet;
        
        if (filter != null)
        {
            query = query.Where(filter);
        }

        if (orderBy != null)
        {
            return orderBy(query).ToList();
        }

        return query.ToList();
    }

    public void Delete(int id)
    {
        var entity = DbSet.Find(id);

        if (entity == null)
        {
            return;
        }
        
        DbSet.Remove(entity);
    }
}