using System.Linq.Expressions;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.Repository;

public class CustomerRepository : IRepository<Customer>
{
    private readonly LiftContext _context;
    private DbSet<Customer> DbSet => _context.Customers;

    public CustomerRepository(LiftContext context)
    {
        _context = context;
    }

    public void Create(Customer entity)
    {
        DbSet.Add(entity);
    }

    public void Edit(Customer entity)
    {
        DbSet.Update(entity);
    }

    public Customer? GetOne(int id)
    {
        return DbSet.Find(id);
    }

    public IEnumerable<Customer> GetMultiple(
        Expression<Func<Customer, bool>>? filter = null, 
        Func<IQueryable<Customer>, IOrderedQueryable<Customer>>? orderBy = null)
    {
        IQueryable<Customer> query = DbSet;
        
        if (filter != null)
        {
            query = query.Where(filter);
        }

        if (orderBy != null)
        {
            return orderBy(query).ToList();
        }

        query.Include(customer => customer.Permits);
        return query.ToList();
    }

    public void Delete(int id)
    {
        var entity = DbSet.Find(id);

        if (entity == null)
        {
            return;
        }

        entity.DeletedAt = DateTime.Now;
        
        Edit(entity);
    }
}