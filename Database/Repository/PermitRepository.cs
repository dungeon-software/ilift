using System.Linq.Expressions;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.Repository;

public class PermitRepository : IRepository<Permit>
{
    private readonly LiftContext _context;
    private DbSet<Permit> DbSet => _context.Permits;

    public PermitRepository(LiftContext context)
    {
        _context = context;
    }

    public void Create(Permit entity)
    {
        DbSet.Add(entity);
    }

    public void Edit(Permit entity)
    {
        DbSet.Update(entity);
    }

    public Permit? GetOne(int id)
    {
        return DbSet.Find(id);
    }

    public IEnumerable<Permit> GetMultiple(
        Expression<Func<Permit, bool>>? filter = null, 
        Func<IQueryable<Permit>, IOrderedQueryable<Permit>>? orderBy = null)
    {
        IQueryable<Permit> query = DbSet;
        
        if (filter != null)
        {
            query = query.Where(filter);
        }

        if (orderBy != null)
        {
            return orderBy(query).ToList();
        }

        return query.ToList();
    }

    public void Delete(int id)
    {
        var entity = DbSet.Find(id);

        if (entity == null)
        {
            return;
        }

        DbSet.Remove(entity);
    }
}